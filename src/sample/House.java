package sample;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.control.Label;
import javafx.util.Duration;

import static sample.Controller.*;

public class House extends Label {

    private Players player;
    private boolean isPlayerBase;
    private House next;
    private SimpleIntegerProperty numberOfMarbles;

    public House(Players player, Boolean isPlayerBase) {
        this.player = player;
        this.isPlayerBase = isPlayerBase;

        this.setStyle("-fx-font-size: 48");

        if (isPlayerBase) {
            this.numberOfMarbles = new SimpleIntegerProperty(0);
        } else {
            this.numberOfMarbles = new SimpleIntegerProperty(6);
            this.setOnMouseClicked(event -> clickOnHouse());
        }
        this.textProperty().bind(numberOfMarbles.asString());
    }

    private void addMarble() {
        this.numberOfMarbles.set(this.numberOfMarbles.get() + 1);
    }

    private void clickOnHouse() {
        if (!activeTurn && this.numberOfMarbles.get() != 0 && (isMoving || this.player == currentPlayer)) {

            activeTurn = true;
            int marbles = this.numberOfMarbles.get();
            this.numberOfMarbles.set(0);
            currentHouse = this;

            // Timelines virker på en mærkelig måde.
            // Det her burde kunne lade sig gøre med et normalt loop, men det virker ikke med et loop.
            // Fordelen er dog at vi får processen at se, millis() kan defineres som 0 for instant
            Timeline gameMove = new Timeline();
            gameMove.setCycleCount(marbles);
            gameMove.getKeyFrames().add(new KeyFrame(Duration.millis(75), e -> {
                currentHouse = currentHouse.next;

                if (currentHouse.isPlayerBase) {
                    if (currentHouse.player != Controller.currentPlayer) {
                        currentHouse = currentHouse.next;
                    }
                }
                currentHouse.addMarble();
            }));
            gameMove.play();

            gameMove.setOnFinished(e -> {
                checkLastHouse();
            });

        } else {
            GameStateReadout.set(currentPlayer + ", those aren't your marbles");
        }
    }

    private void checkLastHouse() {
        activeTurn = false;
        isMoving = false;

        if (currentHouse.isPlayerBase) {
            GameStateReadout.set(currentPlayer + "'s turn again");
        } else {
            if (currentHouse.numberOfMarbles.get() == 1) {
                Controller.changeCurrentPlayer();
                GameStateReadout.set(currentPlayer + "'s turn");
            } else {
                isMoving = true;
                currentHouse.clickOnHouse();
            }
        }
    }

    public void setNext(House next) {
        this.next = next;
    }

}