package sample;

import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    public static Board board;

    public static Players currentPlayer = Players.PLAYER1;

    public static House currentHouse;

    public static SimpleStringProperty GameStateReadout = new SimpleStringProperty("Player 1 - Your turn");

    public static boolean activeTurn = false;
    public static boolean isMoving = false;

    @FXML
    public AnchorPane Pane;
    @FXML
    public Label GameState;
    @FXML
    public static Circle P1H1, P1H2, P1H3, P1H4, P1H5, P1H6;

    public static void changeCurrentPlayer() {
        if (currentPlayer == Players.PLAYER1) {
            currentPlayer = Players.PLAYER2;
        } else currentPlayer = Players.PLAYER1;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        board = new Board(Pane);
        GameState.textProperty().bind(GameStateReadout);
    }
}