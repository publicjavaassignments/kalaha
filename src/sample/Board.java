package sample;

import javafx.geometry.Pos;
import javafx.scene.layout.AnchorPane;

public class Board {

    private AnchorPane pane;
    private House start = null;
    private House end = null;

    public Board(AnchorPane pane) {
        this.pane = pane;
        this.addHouses();
    }

    public void add(House house) {
        if (start == null) {
            start = house;
            end = house;
            house.setNext(house);
        } else {
            end.setNext(house);
            end = house;
            end.setNext(start);
        }
    }

    // TODO: Erstat de har magiske værdier med LayoutX/Y på Circles fra controlleren
    // TODO: frem for at gætte værdierne som her.
    // TODO: Eventuelt bind dem til dem eller noget

    private void addHouses() {
        // P1
        for (int i = 0; i < 6; i++) {
            House newHouse = new House(Players.PLAYER1, false);
            newHouse.setLayoutX(100 * i * 2 + 15);
            newHouse.setLayoutY(603 - 75);
            newHouse.setAlignment(Pos.CENTER);
            newHouse.setPrefWidth(152);
            newHouse.setPrefHeight(152);

            pane.getChildren().add(newHouse);
            this.add(newHouse);
        }

        // P2
        for (int i = 0; i < 6; i++) {
            House newHouse = new House(Players.PLAYER2, false);
            // TODO: Bug, spillet går visuelt den forkerte vej for øverste række grundet koordinatsættet.
            // TODO: Det skal flippes
            newHouse.setLayoutX(100 * i * 2 + 15);
            newHouse.setLayoutY(83 - 75);
            newHouse.setAlignment(Pos.CENTER);
            newHouse.setPrefWidth(152);
            newHouse.setPrefHeight(152);
            pane.getChildren().add(newHouse);
            this.add(newHouse);
        }
        // Bases
        addPlayerBase(7 + 80, 217 + 100, Players.PLAYER1);
        addPlayerBase(993 + 80, 217 + 100, Players.PLAYER2);
    }

    private void addPlayerBase(int layoutX, int layoutY, Players player) {
        House newHouse = new House(player, true);
        newHouse.setLayoutY(layoutY);
        newHouse.setLayoutX(layoutX);
        pane.getChildren().add(newHouse);
        this.add(newHouse);
    }
}